/*
 * https://bitbucket.org/netburnerinterns/robotclient-android/
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

package com.netburner.robotclient;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

interface TCPConnectionCallback
{
    public void onConnected();
}

class TCPClient implements Runnable
{
    private static final String TAG = "NBURNER::TCPClient";
    private static final int SERVER_PORT = 23;
    private static final String SERVER_IP = "192.168.1.1";

    private Socket server;
    private Context context;
    private Handler mainHandler;
    private String status;
    private TCPConnectionCallback callback;
    private boolean connected = false;

    public TCPClient(Context context, TCPConnectionCallback callback) {
        this.context = context;
        this.callback = callback;
        mainHandler = new Handler();
    }

    @Override
    public void run() {
        try {
            Log.d(TAG, "connecting to server");

            InetAddress serverAddress = InetAddress.getByName(SERVER_IP);
            server = new Socket(serverAddress, SERVER_PORT);

        } catch (IOException e) {
            postStatus("error connecting");
            Log.e(TAG, "error connecting to server\n" +  Log.getStackTraceString(e));
        }

        connected = true;
        callback.onConnected();

        postStatus("connected");
        Log.d(TAG, "connected");
    }

    public void write(final byte[] data) {
        try {
            server.getOutputStream().write(data);

        } catch (IOException e) {
            Log.e(TAG, "error writing to server\n" + Log.getStackTraceString(e));
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public void close() {
        if (isConnected()) {
            try {
                Log.d(TAG, "closing socket");
                server.close();
                connected = false;

            } catch (IOException e) {
                Log.e(TAG, "error closing socket\n" + Log.getStackTraceString(e));
            }
        }
    }

    private Runnable showStatus = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
        }
    };

    private void postStatus(String status) {
        this.status = status;
        mainHandler.post(showStatus);
    }
}
