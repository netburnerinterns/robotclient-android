/*
 * https://bitbucket.org/netburnerinterns/robotclient-android/
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

package com.netburner.robotclient;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;

import com.MobileAnarchy.Android.Widgets.Joystick.JoystickMovedListener;
import com.MobileAnarchy.Android.Widgets.Joystick.JoystickView;

import com.arcadeoftheabsurd.absurdengine.RunnerThread;

public class ControllerActivity extends Activity implements SensorEventListener
{
    private static final String TAG = "NBURN:Controller";

    private static final int SENSOR_TYPE = Sensor.TYPE_ROTATION_VECTOR;
    private static final int INIT_DELAY = 5;  // reject the first 5 rotation sensor readings
    private static final int DATA_BYTES = 16; // size of data packages

    private RunnerThread updateThread;

    // UI elements

    private ToggleButton grabButton;
    private TextView orientationView;
    private TextView joyView;
    private JoystickView joystick;

    // rotation sensor

    private SensorManager sensorManager;
    private Sensor rotationSensor;

    private Delegate sensorDelegate;
    private int delay;
    private boolean hasSensor = false;

    private float[] rotDeviceCurr = new float[9];
    private float[] rotDeviceInit = new float[9];

    // robot control

    private float[] orientation = new float[3];
    private boolean grab = false;
    private boolean controlScheme = false;
    private byte joyX = 0;
    private byte joyY = 0;

    private interface Delegate { void func(); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);

        controlScheme = getIntent().getExtras().getBoolean(MainActivity.CONTROL_SCHEME_VAR);

        // set up rotation sensor

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if((rotationSensor = sensorManager.getDefaultSensor(SENSOR_TYPE))
                == null) {
            Toast.makeText(this, "rotation vector sensor unavailable", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "rotation vector sensor unavailable");

        } else {
            sensorDelegate = processOrientationInit;
            hasSensor = true;
            orientationView = (TextView) findViewById(R.id.orientation_text);
        }

        // set up joystick and grab button

        joyView    = (TextView)     findViewById(R.id.joystick_text);
        joystick   = (JoystickView) findViewById(R.id.joystick);
        grabButton = (ToggleButton) findViewById(R.id.grab_btn);

        joystick.setOnJostickMovedListener(new JoystickMovedListener() {
            @Override
            public void OnMoved(int pan, int tilt) {
                joyView.setText("X: " + pan + " Y: " + tilt);
                joyX = (byte)pan;
                joyY = (byte)tilt;
            }

            @Override
            public void OnReleased() {}

            @Override
            public void OnReturnedToCenter() {}
        });

        grabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grab = !grab;
            }
        });

        // start 30 FPS update thread

        updateThread = new RunnerThread(new RunnerThread.UpdateListener() {
            @Override
            public void update() {
                if (MainActivity.connection.isConnected()) {
                    sendControlData();
                }
            }
        });

        updateThread.start();
    }

    /*
     * Function called for the initial garbage sensor readings.
     * When the number of readings reaches INIT_DELAY,
     * stores the initial rotation matrix and
     * sets sensorDelegate to processOrientation
     */
    private Delegate processOrientationInit = new Delegate() {
        @Override
        public void func() {
            if (delay >= INIT_DELAY) {
                rotDeviceInit = rotDeviceCurr.clone();
                sensorDelegate = processOrientation;
            } else {
                delay++;
            }
        }
    };

    /*
     * Function called for each sensor reading after the initial reading
     * Calculates the device's orientation relative to its initial orientation
     */
    private Delegate processOrientation = new Delegate() {
        @Override
        public void func() {
            SensorManager.getAngleChange(orientation, rotDeviceCurr, rotDeviceInit);

            DecimalFormat df = new DecimalFormat("#.#####");

            orientationView.setText(
                    "Yaw: "     + df.format(orientation[0]) +
                    "\nPitch: " + df.format(orientation[1]) +
                    "\nRoll: "  + df.format(orientation[2]));
        }
    };

    /*
     * Rotation sensor callback.
     * Gets rotation matrix in device coordinate system and
     * calls the sensor delegate
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == SENSOR_TYPE) {

            float[] rotWorld = new float[9];

            SensorManager.getRotationMatrixFromVector(rotWorld, event.values.clone());
            SensorManager.remapCoordinateSystem(
                    rotWorld, SensorManager.AXIS_X, SensorManager.AXIS_Y, rotDeviceCurr);

            sensorDelegate.func();
        }
    }

    /*
     * Sends robot control data to server
     */
    private void sendControlData() {
        ByteBuffer buf = ByteBuffer.allocate(DATA_BYTES).order(ByteOrder.BIG_ENDIAN);

        buf.putFloat(orientation[0]);
        buf.putFloat(orientation[1]);
        buf.putFloat(orientation[2]);

        buf.put(joyX);
        buf.put(joyY);

        if (grab) {
            buf.put((byte)0x01);
        } else {
            buf.put((byte)0x00);
        }

        if (controlScheme) {
            buf.put((byte)0x01);
        } else {
            buf.put((byte)0x00);
        }

        MainActivity.connection.write(buf.array());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (hasSensor) {
            sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (updateThread.paused()) {
            updateThread.unpause();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        System.out.println("on pause");

        updateThread.pause();

        if (isFinishing()) {
            System.out.println("is finishing");

            updateThread.finish();

            if (MainActivity.connection != null && MainActivity.connection.isConnected()) {
                MainActivity.connection.close();
            }

            MainActivity.connectionThread.interrupt();
            MainActivity.connectionThread = null;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
