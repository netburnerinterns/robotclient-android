/*
 * https://bitbucket.org/netburnerinterns/robotclient-android/
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

package com.netburner.robotclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class MainActivity extends Activity
{
    private static final String TAG = "NBURNER::Main";

    public static final String CONTROL_SCHEME_VAR = "CONTROL_SCHEME_VAR";
    public static TCPClient connection;
    public static Thread connectionThread;

    private Switch controlSwitch;
    private Button connectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controlSwitch = (Switch) findViewById(R.id.control_switch);

        connection = new TCPClient(this, new TCPConnectionCallback() {
            @Override
            public void onConnected() {
                Intent intent = new Intent(MainActivity.this, ControllerActivity.class);
                intent.putExtra(CONTROL_SCHEME_VAR, controlSwitch.isChecked());
                startActivity(intent);
            }
        });

        connectButton = (Button) findViewById(R.id.connect_btn);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connectionThread == null) {
                    connectionThread = new Thread(connection);
                }

                if (!connectionThread.isAlive()) {
                    connectionThread.start();
                    /*Intent intent = new Intent(MainActivity.this, ControllerActivity.class);
                    startActivity(intent);*/
                }
            }
        });
    }
}
