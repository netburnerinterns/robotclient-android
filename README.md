# __Robot arm remote controller for Android__

Source for an Android TCP client for Ben's [robot arm project](https://bitbucket.org/netburnerinterns/robotarmcontrollerwithwifi).

### Notes

copyright (c) 2015 by  
 
Sam Posner (http://arcadeoftheabsurd.com/)  
and NetBurner, Inc. (http://www.netburner.com/)

Distributed under the BSD 3-Clause License  
See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause